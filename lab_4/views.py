from django.shortcuts import render


# Create your views here.
def index(request):
    response = {}
    return render(request, 'lab_4/lab_4.html', response)
