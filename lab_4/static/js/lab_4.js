let textArea = document.getElementById("textA");
const btn = document.getElementById("btnText");
const chatBody = document.getElementById("chatBody");

const addMsg = () => {
    const pElem = document.createElement('p');
    pElem.className = 'msg-send';
    pElem.innerHTML = textArea.value;
    chatBody.appendChild(pElem);
    textArea.value = "";
};

textArea.addEventListener("keydown", e => {
    const key = e.keyCode ? e.keyCode : e.which;
    if (key === 13) {
        addMsg()
    }
});

btn.addEventListener("click", e => {
    addMsg()
});
