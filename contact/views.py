from django.shortcuts import render, render_to_response
from django.http import HttpResponseRedirect
from .forms import Message_Form
from .models import Message
from django.contrib.auth.decorators import login_required


# Create your views here.
def index(request):
    response = {'message_form': Message_Form}
    return render(request, 'email.html', response)


def message_post(request):
    response = {}
    form = Message_Form(request.POST or None)
    if request.method == 'POST' and form.is_valid():
        response['name'] = request.POST['name'] if request.POST['name'] != "" else "Anonymous"
        response['email'] = request.POST['email'] if request.POST['email'] != "" else "Anonymous"
        response['message'] = request.POST['message']
        message = Message(name=response['name'], email=response['email'], message=response['message'])
        message.save()
        html = 'contact/templates/form_result.html'
        return render(request, 'form_result.html', response)
    else:
        return HttpResponseRedirect('/')


@login_required
def message_table(request):
    response = {}
    message = Message.objects.all()
    response['message'] = message
    html = 'templates/table.html'
    return render(request, 'table.html', response)

# @login_required
# def secret_page(request):
#     return render_to_response("table.html")
