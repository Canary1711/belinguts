from django.db import models
# import datetime
# import pytz

# time_zone = pytz.timezone('Asia/Jakarta')
# date = datetime.datetime.now().date()
# time = datetime.time(23, 59)
# # combite to datetime
# date_time = datetime.datetime.combine(date, time)
# # make time zone aware
# date_time = time_zone.localize(date_time)
# # convert to UTC
# utc_date_time = date_time.astimezone(pytz.utc)
# # get time
# utc_time = utc_date_time.time()


# Create your models here.

class Message(models.Model):
    name = models.CharField(max_length=27)
    email = models.EmailField()
    message = models.TextField()
    created_date = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.message
