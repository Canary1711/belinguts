from django.urls import re_path, path, include
from .views import index, message_post, message_table

app_name = 'contact'

urlpatterns = [
    re_path(r'^$', index, name='contact'),
    re_path(r'^add_message', message_post, name='add_message'),
    re_path(r'^result_table', message_table, name='result_table')
]
